I made this Unity3D project for my bachelor's thesis "Turn-based Japanese Role-Playing game combat mechanics in a Virtual Reality setting" which 
further requires an Oculus Rift along with the Touch Controllers.

The code files are located in ohdear/Assets/Me/Scripts, the executable is called "oh boy.exe".