﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinhsHighlight : MonoBehaviour {

    Quaternion q = new Quaternion();

	// Use this for initialization
	void Start () {
        q = transform.rotation;
        //StartCoroutine(LightItUp());
	}
	
    public void erase()
    {
        Destroy(gameObject);
    }

    void LateUpdate()
    {
        transform.rotation = q;    
    }

    IEnumerator LightItUp()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
}
