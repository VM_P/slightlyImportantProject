﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.Experimental.UIElements;

public class HealthBar : MonoBehaviour
{

    public Slider slider;
    public float HP;

    // Use this for initialization
    void Start()
    {
        //slider.value = HP;
    }

    // Update is called once per frame
    void Update()
    {
        //if (OVRInput.Get(OVRInput.Axis1D.SecondaryIndexTrigger) > 0)
        //      {
        //          Debug.Log("hmmm");
        //          slider.value -= 0.01f;
        //      }
        

    }

    public void showHealth(float hp)
    {
        slider.value = hp;
    }

    public void takeDmg(float dmg)
    {
        slider.value -= dmg;
        //StartCoroutine(dmgAnimation(dmg));
    }

    IEnumerator dmgAnimation(float percentage)
    {
        yield return new WaitForSeconds(1);
    }
}
