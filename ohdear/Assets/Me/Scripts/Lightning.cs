﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DigitalRuby.LightningBolt;

public class Lightning : MonoBehaviour
{

    public LineRenderer lr;
    public Vector3 start;
    public Vector3 end;

    public float damaji;

    void Awake()
    {
        LightningBoltScript lbs = gameObject.GetComponent<LightningBoltScript>();
        lbs.StartPosition = start;
        lbs.EndPosition = end;

        //byte[] noise = { 250 };
        //OVRHaptics.Channels[1].Preempt(new OVRHapticsClip(noise, 1));

        StartCoroutine(Rightning());
    }

    IEnumerator Rightning()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
        TurnSystem ts = GameObject.Find("Overseer").GetComponent<TurnSystem>();
        if (!ts.aoe)
        {
            Selecting.wait = false;
            Selecting.done = true;
        }
    }

    public float dealDamage(GameObject go)
    {
        EnemyBehavior eb = go.GetComponent<EnemyBehavior>();
        if (go.name == "Mutant:Hips")
        {
            eb = go.transform.parent.GetComponent<EnemyBehavior>();
        }

        float ret = eb.takeDMG(damaji, "lightning");
        if (go.name != "Mutant:Hips")
        {
            DiceBehavior db = go.GetComponent<DiceBehavior>();
            db.blindedByLight = true;
        } else
        {
            TheBoss B = go.transform.parent.GetComponent<TheBoss>();
            B.getHit(true);
        }
        if (eb.hitpoints <= 0 && go.name != "Mutant:Hips")
        {
            Transform overlord = go.transform.parent;
            if (go.name == "Mutant:Hips")
                overlord = overlord.transform.parent;
            if (eb.hitpoints <= 0 && go.name != "Mutant:Hips")
            {
                foreach (Transform child in overlord)
                {
                    Destroy(child.gameObject, 0.5f);
                }
            }
            
        }

        PlayerData pd = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerData>();
        pd.setFlames(false);
        Selecting.max = 3;

        return ret;
    }

    
}
