﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHandling : MonoBehaviour {

    public GameObject highlight;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void startHighlight()
    {
        Instantiate(highlight, this.transform);
    }

    public void endHighlight()
    {
        GameObject go = GameObject.Find("PlayerHighlight(Clone)");
        Destroy(go);
    }
}
