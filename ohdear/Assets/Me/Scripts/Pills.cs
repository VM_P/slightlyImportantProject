﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pills : MonoBehaviour
{

    public float amp;
    public string target;

    Vector3 temp = new Vector3();
    Vector3 off = new Vector3();


    // Use this for initialization
    void Start()
    {
        off = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        temp = off;
        temp.y += Mathf.Sin(Time.fixedTime * Mathf.PI) * amp;
        transform.position = temp;
    }

    public void transition()
    {
        StartCoroutine(JUSTDOIT());
    }

    IEnumerator JUSTDOIT()
    {
        yield return new WaitForSeconds(1);
        if (target == "Enth nd")
            Application.Quit();
        else
            SceneManager.LoadScene(target);
    }
}
