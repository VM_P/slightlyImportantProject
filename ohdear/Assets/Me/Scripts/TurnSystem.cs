﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class TurnSystem : MonoBehaviour
{

    public bool regAtk;
    public bool IDGAF;
    public bool aoe;

    private GameObject[] enemies;
    private GameObject player;
    GameObject partner;
    PlayerData pd;
    PartnerBehavior uc;
    float timer;
    int turn; // 0 = playerSide, 1 = enemySide
    int counter;
    bool end;

    public static bool playerTurn;
    public static bool partnerTurn;

    float playerTurnY;
    float enemyTurnY;

    public GameObject playerInd;
    public GameObject enemyInd;
    public GameObject partnerInd;

    // Use this for initialization
    void Start()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        player = GameObject.FindGameObjectWithTag("Player");
        pd = player.GetComponent<PlayerData>();

        turn = 0;
        counter = 0;
        if (aoe)
        {
            turn = 2;
        }
        if (!IDGAF)
        {
            partner = GameObject.FindGameObjectWithTag("Partner");
            if (partner != null)
                uc = partner.GetComponent<PartnerBehavior>();
            nextTurn();
        }
    }

    void nextTurn()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (enemies.Length == 0 || (enemies[0].name == "mutant" && enemies[0].GetComponent<EnemyBehavior>().hitpoints <= 0))
        {
            Debug.Log("All Enemies are gone");
            end = true;
            StartCoroutine(youWon());
            //UnityEditor.EditorApplication.isPlaying = false;
        }

        if (pd.hitpoints <= 0)
        {
            Debug.Log("Game over");
            end = true;
            StartCoroutine(gameOver());
            //UnityEditor.EditorApplication.isPlaying = false;
        }
        if (!end)
        {
            counter++;
            turn++;
            turn %= 3;
            if (turn == 0)
            {
                StartCoroutine(PlayerTurn());
            }

            if (turn == 1)
            {
                StartCoroutine(EnemyTurn());
            }

            if (turn == 2)
            {
                if (partner == null || uc.hp <= 0)
                {
                    nextTurn();
                }
                else
                {
                    StartCoroutine(PartnerTurn());
                }
            }
        }
    }

    IEnumerator PlayerTurn()
    {
        Debug.Log("Playerturn");
        playerInd.SetActive(true);
        enemyInd.SetActive(false);
        partnerInd.SetActive(false);

        Selecting.done = false;
        GameObject UI = GameObject.Find("UI");

        yield return StartCoroutine(waitForPlayerAction());
        yield return new WaitForSeconds(1f);
        pd.deleteDMG();
        nextTurn();
    }

    IEnumerator waitForPlayerAction()
    {
        while (!Selecting.done)
            yield return null;
    }

    IEnumerator PartnerTurn()
    {
        Debug.Log("Partnerturn");
        partnerTurn = true;
        partnerInd.SetActive(true);
        playerInd.SetActive(false);
        enemyInd.SetActive(false);

        uc.startHighlight();
        uc.showMoves();

        yield return waitForPartnerDecision();
        uc.endHighlight();
        uc.hideMoves();
        nextTurn();
    }

    IEnumerator waitForPartnerDecision()
    {
        while (partnerTurn)
            yield return null;
    }

    IEnumerator EnemyTurn()
    {
        Debug.Log("Enemyturn");
        enemyInd.SetActive(true);
        playerInd.SetActive(false);
        partnerInd.SetActive(false);

        for (int i = 0; i < enemies.Length; i++)
        {
            EnemyBehavior eb = enemies[i].GetComponent<EnemyBehavior>();
            DiceBehavior die = enemies[i].GetComponent<DiceBehavior>();
            eb.startHighlight();
            float perc = eb.hitpoints / eb.maxHP;
            // Dice behavior
            if (die != null)
            {
                yield return new WaitForSeconds(1);
                if (perc <= 0.3f)
                {
                    // Self-Destruct
                    Debug.Log("BOOM");
                    die.ani = true;
                    EnemyBehavior.waitUp = true;
                    pd.showPrompt("Self-Destruct", enemies[i].transform.parent.name);

                    yield return StartCoroutine(waitForEnemy());
                    yield return new WaitForSeconds(1);
                    Debug.Log("Killed in one hit");
                    //UnityEditor.EditorApplication.isPlaying = false;
                    SceneManager.LoadScene("ListenToMyStory");
                }
                else if (regAtk)
                {
                    // regular atk
                    pd.showPrompt("Attack", enemies[i].transform.parent.name);
                    EnemyBehavior.waitUp = true;
                    die.atk = true;
                    yield return new WaitForSeconds(2);
                    yield return StartCoroutine(waitForEnemy());
                    yield return new WaitForSeconds(0.5f);
                    pd.deletePrompt();
                }
                else
                {
                    // Wait
                    Debug.Log(enemies[i].transform.parent.name);
                    pd.showPrompt("Wait", enemies[i].transform.parent.name);
                    yield return new WaitForSeconds(2);
                    pd.deletePrompt();
                }
            }
            else if (enemies[i].name == "mutant")
            {
                TheBoss B = enemies[i].GetComponent<TheBoss>();
                EnemyBehavior.waitUp = true;
                B.takeTurn();
                yield return waitForEnemy();
                yield return new WaitForSeconds(1);
                pd.deletePrompt();
            }
            eb.endHighlight();
        }
        nextTurn();
    }

    IEnumerator waitForEnemy()
    {
        while (EnemyBehavior.waitUp)
        {
            yield return null;
        }
    }

    IEnumerator gameOver()
    {
        player.transform.Find("Main Camera").Find("PlayerUI").Find("Game Over").gameObject.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    IEnumerator youWon()
    {
        player.transform.Find("Main Camera").Find("PlayerUI").Find("You Won").gameObject.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

}
