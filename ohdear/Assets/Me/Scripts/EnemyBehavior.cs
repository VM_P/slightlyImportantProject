﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{

    //EnemyStats
    public float maxHP;
    public float hitpoints;

    public GameObject stats;
    public GameObject ligh;
    public bool turn;
    public bool doICare;
    public string element;

    public static bool waitUp;

    Vector3 test;
    GameObject player;

    // Use this for initialization
    void Start()
    {
        TurnSystem ts = GameObject.Find("Overseer").GetComponent<TurnSystem>();
        if (ts.regAtk)
            maxHP = 10;
        hitpoints = maxHP;
        player = GameObject.Find("Playa");
        test = player.transform.position;
    }

    void Update()
    {
        Transform parent = gameObject.transform.parent;
        Vector3 dir = test - parent.position;
        dir.Normalize();
        //Debug.DrawLine(parent.position, parent.position + dir*2);
    }

    public void doMove()
    {
        StartCoroutine(Act());
    }

    public void startHighlight()
    {
        if (name == "mutant")
        {
            GameObject go = Instantiate(ligh).gameObject;
            GameObject floor = GameObject.Find("Floor");
            go.transform.position = new Vector3(transform.position.x, floor.transform.position.y, transform.position.z);
        }
        else
            Instantiate(ligh, gameObject.transform.position, new Quaternion());
    }

    public void endHighlight()
    {
        GameObject go = GameObject.FindGameObjectWithTag("highlight");
        Destroy(go);
    }

    public void createHealthBar()
    {

        GameObject go = GameObject.FindGameObjectWithTag("characterStats");
        if (go == null)
        {
            //Debug.Log("Locked on to " + this.name);
            Transform parent = gameObject.transform.parent;
            GameObject playa = GameObject.Find("Playa");
            Vector3 dir = playa.transform.position - parent.position;
            dir.Normalize();
            Vector3 pos = parent.position + dir - new Vector3(0, 1, 0);

            GameObject goni = Instantiate(stats, parent);
            goni.transform.position = pos;
            if (name == "mutant")
            {
                goni.transform.position += new Vector3(0, 5, 0);
            }
            goni.transform.rotation = Quaternion.LookRotation(goni.transform.position - playa.transform.position, Vector3.up);
            Transform t = goni.transform.Find("HealthBar");
            HealthBar h = t.gameObject.GetComponent<HealthBar>();
            float p = hitpoints / maxHP;
            //Debug.Log(this.name + ": " + p);
            h.showHealth(p);
        }
    }

    public void deleteHealthBar()
    {
        Transform parent = this.transform.parent;

        foreach (Transform child in parent)
        {
            if (child.tag == "characterStats")
            {
                MinhsHighlight mh = child.GetComponent<MinhsHighlight>();
                mh.erase();
            }
        }

    }

    public float takeDMG(float dmg, string attack)
    {
        Transform parent = this.transform.parent;
        PlayerData pd = player.GetComponent<PlayerData>();
        float fac = 1;
        if (pd.concentrated && attack != "atk")
        {
            fac = 2.5f;
        }
        float damage = (int)((dmg * fac) * (1 + PartnerBehavior.buffStacks * 0.25f));
        if (doICare)
        {
            if (attack == element)
            {
                damage -= 55;
            }
        }
        hitpoints -= damage;
        foreach (Transform child in parent)
        {
            if (child.tag == "characterStats")
            {
                GameObject ui = child.gameObject;
                Transform t = ui.transform.Find("HealthBar");
                HealthBar h = t.gameObject.GetComponent<HealthBar>();
                float percentage = damage / maxHP;
                h.takeDmg(percentage);
            }
        }
        return damage;
    }


    IEnumerator Act()
    {
        Instantiate(ligh, gameObject.transform.position, new Quaternion(), this.transform);
        yield return new WaitForSeconds(5);
        GameObject go = GameObject.FindGameObjectWithTag("highlight");
        MinhsHighlight mh = go.GetComponent<MinhsHighlight>();
        mh.erase();
    }
}
